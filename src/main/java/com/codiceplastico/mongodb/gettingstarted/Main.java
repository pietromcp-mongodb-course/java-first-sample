package com.codiceplastico.mongodb.gettingstarted;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Main {
    private static final String ConnectionString = "mongodb://172.31.100.100";

    public static void main(String[] args) {
        try(MongoClient mongoClient = MongoClients.create(ConnectionString)) {
            MongoDatabase db = mongoClient.getDatabase("sample");
            MongoCollection<Person> coll = db.getCollection("java-people", Person.class);
            Person person = new Person(19, "Pietro", "Martinelli");
            coll.insertOne(person);
        }
    }
}
