package com.codiceplastico.mongodb.gettingstarted;

import org.bson.codecs.pojo.annotations.BsonId;

public record Person(@BsonId() int id, String firstName, String lastName) {
}
